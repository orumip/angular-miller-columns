[![pipeline status](https://gitlab.com/orumip/angular-miller-columns/badges/master/pipeline.svg)](https://gitlab.com/orumip/angular-miller-columns/commits/master)
[![coverage report](https://gitlab.com/orumip/angular-miller-columns/badges/master/coverage.svg)](https://gitlab.com/orumip/angular-miller-columns/commits/master)

# Angular Miller Columns

This set of native AngularJS directives allows for easy creation a [miller columns navigation interface](https://en.wikipedia.org/wiki/Miller_columns)

## Install
```bash
npm install --save angular-miller-columns
```

## Usage
Markup:
```html
<div 
    data-miller-columns 
    data-miller-columns-node-provider="nodeProvider"
    data-miller-columns-on-select="onSelectCallback(node)">
</div>
```
In your controller:
```javascript
var nodes = [
    {
        id: 'id1',
        node: 'node1',
        type: 'leaf',
        info: {
            whatever: [you, need]
        }
    },
    {
        id: 'id2',
        node: 'node2',
        type: 'branch'
    }
];

// The node provider must be an instance of MillerColumnsNodeProvider factory
this.nodeProvider = new MillerColumnsNodeProvider();

// Its getChildrenNodes must return "node" children.
// If node === null, you should return root level nodes. The way you retrieve/store your nodes is up to you
// You may directly return data or return a promise.
this.nodeProvider.getChildrenNodes = function(node) {
    return $q(function(resolve) {
        resolve(children);
    });
    // OR 
    // return data;
};

this.onSelectCallback = function(node) {
    // Whatever you need...
};

// You may also inject the MillerColumnsConfiguration value provider in your controller and set some options
// Activate column filter (activated by default)
MillerColumnsConfiguration.filter = true;
// Set filter field placeholder
MillerColumnsConfiguration.filterPlaceholder = 'Filter!';
```

