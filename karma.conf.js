module.exports = function (config) {
    config.set({
        basePath: '',

        preprocessors: {
            '!(*spec|*mock).js': 'coverage'
        },

        files: [
            'node_modules/angular/angular.js',
            'node_modules/angular-mocks/angular-mocks.js',
            'node_modules/angular-sanitize/angular-sanitize.js',
            'miller-columns.js',
            'miller-columns-node-provider-mock.js',
            'miller-columns-spec.js'
        ],

        autoWatch: true,

        frameworks: ['jasmine'],

        browsers: ['PhantomJS'],

        plugins: [
            'karma-junit-reporter',
            'karma-phantomjs-launcher',
            'karma-jasmine',
            'karma-coverage'
        ],

        reporters: ['progress', 'coverage', 'junit'],

        coverageReporter: {
            dir: 'test/coverage/',
            reporters: [
                { type: 'text-summary', subdir: 'text-summary' },
                { type: 'html', subdir: 'html' },
            ]
        },

        junitReporter: {
            outputDir: 'test/out/',
            outputFile: 'unit.xml',
            suite: 'unit'
        }
    })
};
