angular.module('millerColumns.mocks', [])
    .factory('MillerColumnsNodeProviderMock', [
        '$q',
        function($q) {
            function MillerColumnsNodeProvider() {}

            MillerColumnsNodeProvider.prototype.getChildrenNodesPromise = function getChildrenNodesPromise(node) {
                var nodeProvider = this;
                return $q(function(resolver) {
                    return resolver(nodeProvider.getChildrenNodes(node));
                });
            };

            MillerColumnsNodeProvider.prototype.getChildrenNodes = function getChildrenNodes() {};

            MillerColumnsNodeProvider.prototype.refresh = function refresh() {};

            return MillerColumnsNodeProvider;
        }
    ]);