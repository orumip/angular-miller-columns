'use strict';

describe('MillerColumns', function() {
    var fixtureNode1 = {
        id: 'node1',
        node: 'node1',
        type: 'branch'
    };
    var fixtureNode2 = {
        id: 'node2',
        node: 'node2',
        type: 'leaf'
    };
    var fixtureNode3 = {
        id: 'node3',
        node: 'node3',
        type: 'leaf'
    };
    var fixtureNode4 = {
        id: 'node4',
        node: 'node4',
        type: 'branch'
    };

    var fixtureColumn1Nodes = [
        fixtureNode1
    ];
    var fixtureColumn2Nodes = [
        fixtureNode2,
        fixtureNode3,
        fixtureNode4
    ];

    function nodeProvider(node) {
        if (node === null) {
            return fixtureColumn1Nodes;
        } else if (node === fixtureNode1) {
            return fixtureColumn2Nodes;
        }
    }

    // Load require modules
    beforeEach(module('millerColumns'));

    describe('MillerColumns Controllers', function() {
        var $anchorScroll;
        var $controller;
        var $location;
        var $q;
        var $rootScope;
        var MillerColumnsConfiguration;
        var MillerColumnsNodeProviderMock;

        // Load require modules
        beforeEach(module('millerColumns.mocks'));

        // Mocks definitions
        beforeEach(function() {
            module(function ($provide) {
                $provide.factory('MillerColumnsNodeProvider', function() {return MillerColumnsNodeProviderMock;});
            });
        });

        // Angular dependencies injection
        beforeEach(inject(function($injector) {
            $controller = $injector.get('$controller');
            $location = $injector.get('$location');
            $q = $injector.get('$q');
            $rootScope = $injector.get('$rootScope');
        }));

        // Mocks injection
        beforeEach(inject(function($injector) {
            MillerColumnsConfiguration = $injector.get('MillerColumnsConfiguration');
            MillerColumnsNodeProviderMock = $injector.get('MillerColumnsNodeProviderMock');
            $anchorScroll = jasmine.createSpy('$anchorScroll');
        }));

        // Controllers description
        describe('MillerColumnsController', function() {
            var $scope;
            var ctrl;

            beforeEach(function() {
                $scope = $rootScope.$new();
                $scope.millerColumnsNodeProvider = new MillerColumnsNodeProviderMock();
                $scope.millerColumnsNodeProvider.getChildrenNodes = nodeProvider;
                $scope.millerColumnsOnSelect = function(node) {};

                ctrl = $controller('MillerColumnsController', {
                    $scope: $scope
                });
                $scope.$apply();
            });

            beforeEach(function() {
                spyOn($scope, 'millerColumnsOnSelect');
                spyOn(ctrl, 'addColumn').and.callThrough();
                spyOn($scope.millerColumnsNodeProvider, 'getChildrenNodesPromise').and.callThrough();
            });

            it('should initialize columns', function() {
                expect(ctrl.columns.length).toBe(1);
            });

            describe('addColumn function', function() {
                it('should fill a new column with provided nodes', function() {
                    var expectedColumns = [
                        {
                            parent: null,
                            nodes: fixtureColumn1Nodes,
                            selected: null,
                            filter: ''
                        },
                        {
                            parent: fixtureNode1,
                            nodes: fixtureColumn2Nodes,
                            selected: null,
                            filter: ''
                        }
                    ];

                    ctrl.addColumn(fixtureNode1, fixtureColumn2Nodes);

                    expect(ctrl.columns).toEqual(expectedColumns);
                });
            });

            describe('init function', function() {
                it('should set the nodes provider refresh method', function() {
                    $scope.millerColumnsNodeProvider.refresh = null;

                    ctrl.init();

                    expect($scope.millerColumnsNodeProvider.refresh).toBe(ctrl.refreshColumns);
                });

                it('should call getChildrenNodesPromise and call addColumns with the result', function() {
                    ctrl.init();
                    $scope.$digest();

                    expect($scope.millerColumnsNodeProvider.getChildrenNodesPromise).toHaveBeenCalledWith(null);
                    expect(ctrl.addColumn).toHaveBeenCalledWith(null, fixtureColumn1Nodes);
                });
            });

            describe('select function', function() {
                it('should trim non-needed columns', function() {
                    // Add a column so the total number of columns is 2 (1 is already added by the node provider mock)
                    ctrl.addColumn(fixtureNode1, fixtureColumn2Nodes);

                    ctrl.select(fixtureNode1, 0);

                    expect(ctrl.columns.length).toBe(1);
                });

                it('should call the nodes provider and call addColumns with the result', function() {
                    ctrl.select(fixtureNode1, 0);
                    $scope.$digest();

                    expect($scope.millerColumnsNodeProvider.getChildrenNodesPromise).toHaveBeenCalledWith(fixtureNode1);
                    expect(ctrl.addColumn).toHaveBeenCalledWith(fixtureNode1, fixtureColumn2Nodes);
                });

                it('should not call the nodes provider if the selected node is a leaf', function() {
                    ctrl.addColumn(fixtureNode1, fixtureColumn2Nodes);

                    ctrl.select(fixtureNode2, 1);
                    $scope.$digest();

                    expect($scope.millerColumnsNodeProvider.getChildrenNodesPromise).not.toHaveBeenCalled();
                });
            });

            describe('refreshColumns function', function() {
                it('should call the nodes provider for each column and update the nodes', function() {
                    var expectedColumns = [
                        {
                            parent: null,
                            nodes: fixtureColumn1Nodes,
                            selected: null,
                            filter: ''
                        },
                        {
                            parent: fixtureNode1,
                            nodes: fixtureColumn2Nodes,
                            selected: null,
                            filter: ''
                        }
                    ];

                    ctrl.addColumn(fixtureNode1, fixtureColumn2Nodes);

                    ctrl.refreshColumns();
                    $scope.$digest();

                    expect($scope.millerColumnsNodeProvider.getChildrenNodesPromise).toHaveBeenCalledTimes(2);
                    expect($scope.millerColumnsNodeProvider.getChildrenNodesPromise).toHaveBeenCalledWith(null);
                    expect($scope.millerColumnsNodeProvider.getChildrenNodesPromise).toHaveBeenCalledWith(fixtureNode1);

                    expect(ctrl.columns).toEqual(expectedColumns);
                });
            });
        });

        describe('MillerColumnController', function() {
            var $scope;
            var ctrl;

            var column_data = {
                nodes: [
                    fixtureNode1,
                    fixtureNode2,
                    fixtureNode3
                ],
                selected: fixtureNode2.id
            };

            beforeEach(function() {
                $scope = $rootScope.$new();
                $scope.millerColumnsCtrl = {
                    columns:[],
                    select: function(node) {},
                    millerColumnsOnSelect: function(node) {}
                };
                $scope.millerColumnId = 0;
                $scope.millerColumnData = column_data;

                MillerColumnsConfiguration.filterPlaceholder = 'Filter!';

                ctrl = $controller('MillerColumnController', {
                    $scope: $scope,
                    $location: $location,
                    $anchorScroll: $anchorScroll,
                    MillerColumnsConfiguration: MillerColumnsConfiguration
                });
            });

            beforeEach(function() {
                spyOn($scope.millerColumnsCtrl, 'select');
                spyOn($location, 'hash');
            });

            it('should read the configuration from the MillerColumnsConfiguration value provider', function() {
                expect(ctrl.configuration).toBe(MillerColumnsConfiguration);
            });

            describe('init function', function() {
                it('should scroll to the right to make sure the newly created column is visible', function() {
                    ctrl.init();

                    expect($location.hash).toHaveBeenCalledWith('miller-columns-end');
                    expect($anchorScroll).toHaveBeenCalled();
                });
            });

            describe('isNodeActive function', function() {
                it('should return true if the node is active', function() {
                    expect(ctrl.isNodeActive(fixtureNode2)).toBe(true);
                });

                it('should return false if the node is not active', function() {
                    expect(ctrl.isNodeActive(fixtureNode1)).toBe(false);
                });
            });

            describe('selectNode function', function() {
                it('should set the active node and call the parent controller select method', function() {
                    ctrl.selectNode(fixtureNode1);

                    expect(ctrl.data.selected).toBe(fixtureNode1.id);
                    expect($scope.millerColumnsCtrl.select).toHaveBeenCalledWith(fixtureNode1, 0);
                });
            });
        });
    });

    describe('MillerColumns Directives', function() {
        var $compile;
        var $q;
        var $rootScope;
        var $scope;

        var MillerColumnsNodeProviderMock;

        var element;

        // Load required modules
        beforeEach(module('millerColumns.mocks'));

        // Mocks definitions
        beforeEach(function() {
            module(function ($provide) {
                $provide.factory('MillerColumnsNodeProvider', function() {return MillerColumnsNodeProviderMock;});
            });
        });

        // Directives description
        describe('millerColumns', function() {
            // see http://stackoverflow.com/questions/17533052/how-do-you-mock-directives-to-enable-unit-testing-of-higher-level-directive#20951085
            beforeEach(module('millerColumns', {
                millerColumnDirective: {}
            }));

            // Angular dependencies injection
            beforeEach(inject(function($injector) {
                $compile = $injector.get('$compile');
                $q = $injector.get('$q');
                $rootScope = $injector.get('$rootScope');
            }));

            // Mocks injection
            beforeEach(inject(function($injector) {
                MillerColumnsNodeProviderMock = $injector.get('MillerColumnsNodeProviderMock');
            }));

            beforeEach(function() {
                $scope = $rootScope.$new();

                $scope.millerColumnsNodeProvider = new MillerColumnsNodeProviderMock();
                $scope.millerColumnsNodeProvider.getChildrenNodes = nodeProvider;
                $scope.millerColumnsOnSelect = function(node) {};

                spyOn($scope.millerColumnsNodeProvider, 'getChildrenNodes').and.callThrough();
                spyOn($scope, 'millerColumnsOnSelect');

                element = $compile('<div data-miller-columns data-miller-columns-node-provider="millerColumnsNodeProvider" data-miller-columns-on-select="select(node)"></div>')($scope);
                $scope.$digest();
            });

            it("should replace the element with appropriate markup", function() {
                expect(element[0].querySelectorAll('[data-miller-column]').length).toBe(1);
            });

            it("should call the node provider", function() {
                expect($scope.millerColumnsNodeProvider.getChildrenNodes).toHaveBeenCalledWith(null);
            });

            it("should display a new column when it is added", function() {
                element.controller('millerColumns').addColumn(fixtureColumn2Nodes);
                element.scope().$digest();
                expect(element[0].querySelectorAll('[data-miller-column]').length).toBe(2);
            });
        });

        describe('millerColumn', function() {
            // Angular dependencies injection
            beforeEach(inject(function($injector) {
                $compile = $injector.get('$compile');
                $q = $injector.get('$q');
                $rootScope = $injector.get('$rootScope');
            }));

            beforeEach(function() {
                // see https://demisx.github.io/angularjs/unit-testing/2014/10/28/unit-testing-angular-child-directive-that-requires-parent.html
                // Mock parent directive controller
                var millerColumnsControllerMock = {
                    millerColumnsOnSelect: function() {},
                    select: function() {}
                };

                var mockChildElement = angular.element('<div data-miller-columns></div>');
                mockChildElement.data('$millerColumnsController', millerColumnsControllerMock);

                // Create the child directive
                $scope = $rootScope.$new();

                $scope.id = 1;
                $scope.nodes = {
                    selected: null,
                    nodes: fixtureColumn2Nodes
                };

                element = angular.element('<div data-miller-column data-miller-column-id="id" data-miller-column-data="nodes"></div>');
                mockChildElement.append(element);
                element = $compile(element)($scope);
                $scope.$digest();
            });

            beforeEach(function() {
                spyOn(element.controller('millerColumn'), 'selectNode').and.callThrough();
            });

            it("should replace the element with appropriate markup", function() {
                expect(element[0].querySelectorAll('.miller-columns-node').length).toBe(3);
            });

            it("should select the clicked node", function() {
                element[0].querySelectorAll('.miller-columns-node a')[0].click();
                $scope.$digest();
                expect(element.controller('millerColumn').selectNode).toHaveBeenCalledWith(fixtureNode2);
                expect(angular.element(element[0].querySelectorAll('.miller-columns-node')[0]).hasClass('active')).toBe(true);
            });
        });
    });

    describe('MillerColumnsNodeProvider', function() {
        var $q;
        var MillerColumnsNodeProvider;
        var nodeProviderInstance;

        function nodeProviderPromise(node) {
            return $q(function(resolver) {
                if (node === null) {
                    resolver(fixtureColumn1Nodes);
                } else if (node === fixtureNode1) {
                    resolver(fixtureColumn2Nodes);
                }
            });
        }

        // Angular dependencies injection
        beforeEach(inject(function($injector) {
            $q = $injector.get('$q');
            MillerColumnsNodeProvider = $injector.get('MillerColumnsNodeProvider');
        }));

        beforeEach(function() {
            nodeProviderInstance = new MillerColumnsNodeProvider();

            nodeProviderInstance.getChildrenNodes = nodeProvider;
            spyOn(nodeProviderInstance, 'getChildrenNodes').and.callThrough();
        });

        describe('getChildrenNodesPromise method', function() {
            it('should wrap the getChildrenNodes method return in a promise', function() {
                var childrenNodesPromise = nodeProviderInstance.getChildrenNodesPromise(fixtureNode1);

                expect(childrenNodesPromise.then).toBeDefined();
                expect(nodeProviderInstance.getChildrenNodes).toHaveBeenCalledWith(fixtureNode1);
            });

            it('should not wrap the getChildrenNodes method return in a promise if it already returns a promise', function() {
                nodeProviderInstance.getChildrenNodes = nodeProviderPromise;
                spyOn(nodeProviderInstance, 'getChildrenNodes').and.callThrough();

                var childrenNodesPromise = nodeProviderInstance.getChildrenNodesPromise(fixtureNode1);

                expect(childrenNodesPromise.then).toBeDefined();
                expect(nodeProviderInstance.getChildrenNodes).toHaveBeenCalledWith(fixtureNode1);
            });

            it('should throw an exception if no node provider is defined', function() {
                nodeProviderInstance.getChildrenNodes = null;

                expect(nodeProviderInstance.getChildrenNodesPromise).toThrowError('[MillerColumnsController] No node provider is defined');
            });
        });
    });
});
